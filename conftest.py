#place for my fixtures
from MyProjectDB import ProjectDB
import pytest

@pytest.fixture(scope='module')
def db():
   print("------setup------")
   fixture_db = ProjectDB()
   cur = fixture_db.connect()
   yield fixture_db
   print("-------teardown-------")
   fixture_db.close()




