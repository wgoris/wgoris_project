import pyautogui as gui, time as t
import sys

class MyBrowser:

    def __init__(self, browser_name ="", x=0, y=0, duration=2):
        if x < 0 or y < 0:
            print("x: ", x)
            print("y: ", y)
            raise ValueError("Can't have negative coordinates now can we.")
        if browser_name != "Firefox" and browser_name != "Chrome":
            raise ValueError("What is this browser you speak of.")

        self._name = browser_name
        self._x = x
        self._y = y
        self._duration = duration
    
    @property
    def browser(self):
        return self._name
    
    @property
    def x(self):
        return self._x
    
    @property
    def y(self):
        return self._y
    
    def navigate(self, _x, _y):
        new_x = _x
        new_y = _y
        if new_x < 0 or new_y < 0:
            raise ValueError("Can't have negative coordinates now can we.")
        self._x = new_x
        self._y = new_y
         
        try:
            gui.moveTo(self._x, self._y, self._duration)
            gui.click()
            t.sleep(2)
        except expression as identifier:
            pass
    
    #interval is used to make it human readable
    def enter_input(self):
        gui.moveTo(424, 106, 3)
        gui.click()
        gui.typewrite('google', interval=0.25)
        gui.press('enter')
        t.sleep(5)
    
    def close_browser(self):
        gui.moveTo(1908, 41, self._duration)
        gui.click()

    
