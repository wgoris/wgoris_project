import pytest

from pytest_bdd import scenario, parsers, given, when , then

from browser import MyBrowser

from MyProjectDB import ProjectDB

#scenarios('browser.feature')

EXTRA_TYPES = {
    'Number': int
}



f = 'Firefox'
c = 'Chrome'

@pytest.mark.parametrize(
    'browser_param',
    [
        (f),
        (c)
    ]
)

def test_parametrize(browser_param):
    print("browser_param: ", browser_param)
    assert browser_param == 'Firefox' or browser_param =='Chrome'

@scenario('browser.feature', 'Browser scenario')
def test_func(browser):
    pass

@given(parsers.cfparse('I have "{id:Number}" as my browser', extra_types=EXTRA_TYPES))
@given('I have "<id>" as my browser')
def browser(db, id):
    name = db.get_browser(id)
    x = db.get_browser_x(id)
    y = db.get_browser_y(id)
    print("----------browser returned-----------", name)
    return MyBrowser(browser_name=name, x=x, y=y, duration=2)


@when("I navigate to icon")
def test_go_to_browser(browser):
    print("--------------I AM NAVIGATING-------------")
    browser.navigate(browser.x, browser.y)
    browser.enter_input()

@then('A browser be should open')
def test_if_browser_is_open(browser):
    browser.close_browser()
    
