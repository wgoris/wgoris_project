Project Title
Automated GUI Testframework

Getting Started
git clone git@bitbucket.org:wgoris/wgoris_project.git once environment is setup

Prerequisites

To match my setup exactly:
Virtual Machine
Ubuntu 18.04


libraries packages:

install pip3

sudo apt-get install -y python3-pip

to install package

pip3 install package_name

setup few more packages and development tools

sudo apt-get install build-essential libssl-dev libffi-dev python-dev

If you want virtual environment:

install venv module

sudo apt-get install -y python3-venv

create a enviornments

python3 -m venv project_env

Installing pytest:

pip install -U pytest

check for your install version

pytest --version should be 5.x.y

After confirming version install pytest-bdd plugin

pip pytest-bdd 3.2.1

to install pyautogui

python3 -m pip install pyautogui

to install msqlconnector

pip install mysql-connector-python

Running the tests

run pytest in root directory tests
run pytest-bdd for feature file output


Versioning
Git

Authors
Wouter Goris

License

Acknowledgments
Would not have been possible without ttl