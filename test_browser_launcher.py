import pyautogui as gui, time as t
import sys
gui.FAILSAFE = True

#using x y coördinates
def test_browser_through_search_applications():
    screenWidth, screenHeight = gui.size()
    gui.moveTo(1,screenHeight)
    gui.click()

    gui.typewrite('Firefox', interval=0.25)
    gui.press('enter')
    t.sleep(5)
    gui.moveTo(424,106)
    gui.click()
    gui.typewrite('google', interval=0.25)
    gui.press('enter')
    t.sleep(5)
    gui.moveTo(1908,41)
    gui.click()


def test_close_browser_through_keyboard_input():
    gui.moveTo(29, 63, 2)
    gui.click()
    t.sleep(2)
    gui.press('alt')
    t.sleep(1)
    gui.press('f')
    t.sleep(1)
    gui.press('q')


#using screenshot recognition
#couldNotImportPyScreeze
#needs to be implemented
def test_browser_through_screenshot_recognition():
    location = gui.locateCenterOnScreen('/home/wouter/Documents/projectrepos/awesome_python_project/trash.png')
    t.sleep(2)
    if location is None:
        gui.alert('Can not find image')
    else:
        t.sleep(2)
        gui.click(location)
    #assert False

#Build in screenshot doesnt work either for location detection
def test_screenshot_build_in_fnc():
    image = gui.screenshot(region=(0,0, 300, 400))
    t.sleep(2)
    if image is None:
        gui.alert('Nonetype')
        assert False
    else:
        gui.alert('atleast its not None')
        t.sleep(2)
        locatedImg = gui.locateOnScreen(image)
        t.sleep(2)
        if locatedImg is None:
            False
        else:
            assert True
