import mysql.connector
from mysql.connector import Error

#making connection when instantiating db class
class ProjectDB:
    def __init__(self):
        self._connection = mysql.connector.connect(host='tracks.ttl.be',
                                     database='wouterproject',
                                     user='wg',
                                     password='wgpw')
            
    #adding cursor to class variable
    def connect(self):
        self._cur =  self._connection.cursor()
    
    #querying database to return browser by id
    def get_browser(self, id):
        self._cur.execute("select name FROM browser WHERE browser.browser_id ="+str(id))

        
        browser = self._cur.fetchall()
        print("Browser: ", browser)
        
        for x in browser:
            name = x[0]
            print("browser: ", name)
            return name
    
    #get x coordinate by id
    def get_browser_x(self, id):
        self._cur.execute("select x FROM browser_xy WHERE browser_id ="+str(id))
        browser_x = self._cur.fetchall()

        for x in browser_x:
            x = x[0]
            return x

    #get y coordinate by id
    def get_browser_y(self, id):
        self._cur.execute("select y FROM browser_xy WHERE browser_id ="+str(id))
        browser_y = self._cur.fetchall()

        for y in browser_y:
            y = y[0]
            return y


    def get_user(self, name):
        self._cur.execute("")

    def close(self):
        pass
